package vitalii.gotonews.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class News(
    val id: String,
    val sectionId: String,
    val section: String,
    val date: String,
    val webTitle: String,
    val webUrl: String,
    val byline: String,
    val thumbnail: String? = null,
    val bodyText: String,
    // TODO: 29.04.2022 use val
    var isBookmark: Boolean = false
) : Parcelable