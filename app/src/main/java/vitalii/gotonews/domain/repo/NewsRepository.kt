package vitalii.gotonews.domain.repo

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import vitalii.gotonews.domain.model.News

interface NewsRepository {

    fun getNewsByTitle(title: String): Flow<PagingData<News>>

    fun getNewsBySectionIdAndTitle(sectionId: String?, title: String): Flow<PagingData<News>>

    fun getBookmarksByTitle(title: String): Flow<PagingData<News>>

    fun getHistoryByTitle(title: String): Flow<PagingData<News>>

    suspend fun addToBookmarks(news: News)

    suspend fun removeFromBookmarks(newsId: String)

    suspend fun addToHistory(news: News)

    suspend fun removeFromHistory(newsId: String)
}