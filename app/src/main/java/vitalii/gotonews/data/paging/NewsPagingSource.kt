package vitalii.gotonews.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import vitalii.gotonews.domain.model.News

typealias NewsPageLoader = suspend (pageIndex: Int, perPage: Int) -> List<News>

class NewsPagingSource(
    private val newsPageLoader: NewsPageLoader,
    private val perPage: Int
) : PagingSource<Int, News>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, News> {
        val pageIndex = params.key ?: 1
        return try {
            val news = newsPageLoader.invoke(pageIndex, perPage)
            return LoadResult.Page(
                data = news,
                prevKey = if (pageIndex == 1) null else pageIndex - 1,
                nextKey = if (news.size == perPage) pageIndex + 1 else null
            )
        } catch (e: Exception) {
            LoadResult.Error(
                throwable = e
            )
        }
    }

    override fun getRefreshKey(state: PagingState<Int, News>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPosition) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }
}