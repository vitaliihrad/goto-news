package vitalii.gotonews.data.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import vitalii.gotonews.data.paging.NewsPageLoader
import vitalii.gotonews.data.paging.NewsPagingSource
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.domain.repo.NewsRepository
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random

@Singleton
class MockNewsRepositoryImpl @Inject constructor() : NewsRepository {

    private val thumbnails = listOf(
        null,
        "https://region-news.kr.ua/wp-content/uploads/2020/07/23.jpg",
        "https://e-tender.ua/storage/editor/fotos/5cf22a89c4b51f509fbd71ced6ab71b1_1538981369.jpeg",
        "https://www.balernovt.org.uk/wp2/wp-content/uploads/2016/06/news.jpg",
        null
    )
    private val history = mutableListOf<News>()
    private val bookmarks = mutableListOf<News>()
    private lateinit var historyPagingSource: NewsPagingSource

    override fun getNewsByTitle(title: String) =
        getResultList(title = title, data = createNewsList())

    override fun getNewsBySectionIdAndTitle(sectionId: String?, title: String) =
        getResultList(title = title, sectionId = sectionId, data = createNewsList())

    override fun getHistoryByTitle(title: String) =
        getResultList(title = title, data = history, isHistory = true)

    override fun getBookmarksByTitle(title: String) = getResultList(title = title, data = bookmarks)

    override suspend fun addToHistory(news: News) {
        history.add(news)
    }

    override suspend fun removeFromHistory(newsId: String) {
        history.removeIf { news ->
            news.id == newsId
        }
        historyPagingSource.invalidate()
        Timber.d("Remove from history$history")
    }

    override suspend fun addToBookmarks(news: News) {
        bookmarks.add(news)
        Timber.d("add to bookmarks$bookmarks")
    }

    override suspend fun removeFromBookmarks(newsId: String) {
        bookmarks.removeIf { news ->
            news.id == newsId
        }
    }

    private fun getResultList(
        title: String,
        data: List<News>,
        sectionId: String? = null,
        isHistory: Boolean = false
    ): Flow<PagingData<News>> {
        val news = searchNews(title, sectionId, data)
        return Pager(
            config = PAGING_CONFIG,
            pagingSourceFactory = {
                NewsPagingSource(getLoader(news), PAGE_SIZE).apply {
                    if (isHistory) historyPagingSource = this
                }
            }
        ).flow
    }

    private fun searchNews(title: String, sectionId: String?, data: List<News>) =
        if (sectionId == null) {
            data
        } else {
            data.filter {
                it.sectionId == sectionId
            }
        }.filter {
            it.webTitle.contains(title, true)
        }

    private fun createNewsList() = List(NEWS_AMOUNT) { id: Int ->
        id.toString().let { data ->
            News(
                id = data,
                sectionId = data,
                section = data,
                date = Date().toString(),
                webTitle = data,
                webUrl = data,
                byline = data,
                thumbnail = thumbnails[Random.nextInt(0, thumbnails.size)],
                bodyText = data,
            )
        }
    }

    companion object {

        const val PAGE_SIZE = 1
        const val NEWS_AMOUNT = 21
        val PAGING_CONFIG = PagingConfig(
            pageSize = PAGE_SIZE,
            enablePlaceholders = false,
            initialLoadSize = PAGE_SIZE
        )

        fun getLoader(data: List<News>): NewsPageLoader {
            val loader: NewsPageLoader = loader@{ pageIndex, perPage ->
                val startIndex = (pageIndex - 1) * perPage
                val endIndex = pageIndex * perPage
                if (data.isEmpty()) {
                    return@loader emptyList()
                }
                data.subList(startIndex, endIndex)
            }
            return loader
        }
    }
}