package vitalii.gotonews.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.commit

fun <T : Fragment> Fragment.openFragment(
    container: Int,
    fragment: T,
    fragmentTag: String,
    addToBackStack: Boolean
) {
    parentFragmentManager.commit {
        replace(
            container,
            fragment,
            fragmentTag
        )
        if (addToBackStack) addToBackStack(fragmentTag)
    }
}