package vitalii.gotonews.ui

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import vitalii.gotonews.R
import vitalii.gotonews.databinding.ActivityMainBinding
import vitalii.gotonews.ui.main.bookmarks.BookmarksFragment
import vitalii.gotonews.ui.main.history.HistoryFragment
import vitalii.gotonews.ui.main.newslist.NewsListFragment

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val bottomNavigation by lazy {
        binding.bottomNavigationView
    }
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        setListeners()
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.main_container, NewsListFragment(), NewsListFragment.TAG)
            }
        }
    }

    private fun initUi() {
        setSupportActionBar(findViewById(R.id.toolbar))
        bottomNavigation.selectedItemId = R.id.recent_item_bottom_navigation
    }

    private fun setListeners() {
        bottomNavigation.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.history_item_bottom_navigation -> openFragment(
                    HistoryFragment.newInstance(),
                    HistoryFragment.TAG,
                    R.string.text_history_item_bottom_navigation
                )

                R.id.recent_item_bottom_navigation -> openFragment(
                    NewsListFragment.newInstance(),
                    NewsListFragment.TAG,
                    R.string.text_recent_item_bottom_navigation
                )
                R.id.bookmarks_item_bottom_navigation -> openFragment(
                    BookmarksFragment.newInstance(),
                    BookmarksFragment.TAG,
                    R.string.text_saved_item_bottom_navigation
                )
            }
            true
        }
    }

    private fun <T : Fragment> openFragment(
        fragment: T,
        tag: String,
        @StringRes toolbarTitle: Int
    ) {
        val isOpened = supportFragmentManager.findFragmentByTag(tag)?.isVisible ?: false
        Timber.d("Is Opened: $isOpened")
        if (!isOpened) {
            findViewById<Toolbar>(R.id.toolbar).setTitle(toolbarTitle)
            supportFragmentManager.commit {
                replace(R.id.main_container, fragment, tag)
            }
        }
    }
}