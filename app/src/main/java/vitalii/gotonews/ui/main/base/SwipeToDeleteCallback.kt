package vitalii.gotonews.ui.main.base

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import vitalii.gotonews.domain.model.News

class SwipeToDeleteCallback(
    private val adapter: NewsAdapter,
    private val removeCallback: (news: News?) -> Unit
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        removeCallback(adapter.getCurrentItem(viewHolder.absoluteAdapterPosition))
    }
}