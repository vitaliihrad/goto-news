package vitalii.gotonews.ui.main.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import vitalii.gotonews.databinding.ItemNewsBinding
import vitalii.gotonews.domain.model.News

class NewsAdapter(
    private val onClick: (position: Int, type: NewsViewHolder.ClickType) -> Unit
) : PagingDataAdapter<News, NewsViewHolder>(NewsDiffCallback()) {

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(getItem(position) ?: return)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemNewsBinding.inflate(inflater, parent, false)
        return NewsViewHolder(binding, onClick)
    }

    fun getCurrentItem(position: Int) = getItem(position)
}