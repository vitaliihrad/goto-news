package vitalii.gotonews.ui.main.base

import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import vitalii.gotonews.R
import vitalii.gotonews.databinding.FragmentNewsListBinding
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.ui.base.BaseFragment
import vitalii.gotonews.ui.details.DetailsFragment
import vitalii.gotonews.util.hideKeyboard
import vitalii.gotonews.util.openFragment

abstract class BaseNewsFragment : BaseFragment<FragmentNewsListBinding>() {

    abstract val viewModel: BaseNewsViewModel

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentNewsListBinding =
        FragmentNewsListBinding::inflate

    open val onItemClickListener: (position: Int, type: NewsViewHolder.ClickType) -> Unit =
        { position, type ->
            adapter.getCurrentItem(position)?.let { news ->
                when (type) {
                    NewsViewHolder.ClickType.BOOKMARK -> updateBookmark(news, position)
                    NewsViewHolder.ClickType.DETAILS -> showDetails(news)
                }
            }
        }

    val adapter by lazy { NewsAdapter(onItemClickListener) }

    override fun initUI() {
        super.initUI()
        setHasOptionsMenu(true)
        setAdapter()
        getNews()
    }

    override fun setObserving() {
        super.setObserving()
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.newsFlowResult.collectLatest(adapter::submitData)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu, menu)
        val search = menu.findItem(R.id.toolbar_search_item)
        val searchView = search.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String?): Boolean {
                Timber.d("NewText = $newText")
                newText?.let(::getNews)
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                Timber.d("Query = $query")
                hideKeyboard()
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    open fun getNews(title: String = "") {
        viewModel.searchNewsByTitle(title)
    }

    open fun updateBookmark(news: News, position: Int) {
        news.isBookmark = !news.isBookmark
        if (news.isBookmark) {
            viewModel.addToBookmarks(news = news)
        } else {
            viewModel.removeFromBookmarks(newsId = news.id)
        }
        adapter.notifyItemChanged(position)
        Timber.d(news.toString())
    }

    open fun setAdapter() {
        with(adapter) {
            binding.newsRecyclerView.adapter = this.withLoadStateHeaderAndFooter(
                header = NewsLoadStateAdapter(),
                footer = NewsLoadStateAdapter()
            )
            addLoadStateListener { state ->
                binding.progressBar.isVisible = state.refresh == LoadState.Loading
            }
        }
    }

    open fun showDetails(news: News) {
        Timber.d(news.toString())
        openFragment(
            R.id.main_container,
            DetailsFragment.newInstance(news = news),
            DetailsFragment.TAG,
            true
        )
    }
}