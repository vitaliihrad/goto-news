package vitalii.gotonews.ui.main.bookmarks

import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.ui.main.base.BaseNewsFragment

@AndroidEntryPoint
class BookmarksFragment : BaseNewsFragment() {

    override val viewModel by viewModels<BookmarksViewModel>()

    override fun showDetails(news: News) {
        super.showDetails(news)
        viewModel.addNewsToHistory(news)
    }

    companion object {

        val TAG: String = BookmarksFragment::class.java.name

        fun newInstance() = BookmarksFragment()
    }
}