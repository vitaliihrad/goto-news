package vitalii.gotonews.ui.main.base

data class Filter(
    val sectionId: String?,
    val title: String
)