package vitalii.gotonews.ui.main.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.RecyclerView
import coil.load
import vitalii.gotonews.R
import vitalii.gotonews.databinding.ItemNewsBinding
import vitalii.gotonews.domain.model.News

class NewsViewHolder(
    private val binding: ItemNewsBinding,
    private val onClick: (position: Int, type: ClickType) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        with(binding) {
            bookmarkImageView.setOnClickListener {
                onClick(bindingAdapterPosition, ClickType.BOOKMARK)
            }
            itemConstraintLayout.setOnClickListener {
                onClick(bindingAdapterPosition, ClickType.DETAILS)
            }
        }
    }

    fun bind(news: News) {
        with(binding) {
            newsHeading.text = news.webTitle
            newsDatePublication.text = news.date
            bookmarkImageView.setImageResource(
                if (news.isBookmark) {
                    R.drawable.ic_bookmark_true
                } else {
                    R.drawable.ic_bookmark_false
                }
            )
            newsImageView.load(news.thumbnail) {
                placeholder(ColorDrawable(Color.TRANSPARENT))
            }
        }
    }

    enum class ClickType {
        BOOKMARK,
        DETAILS
    }
}