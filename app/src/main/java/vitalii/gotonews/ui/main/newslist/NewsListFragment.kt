package vitalii.gotonews.ui.main.newslist

import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber
import vitalii.gotonews.R
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.ui.main.base.BaseNewsFragment

@AndroidEntryPoint
class NewsListFragment : BaseNewsFragment() {

    override val viewModel by viewModels<NewsListViewModel>()

    override fun showDetails(news: News) {
        super.showDetails(news)
        viewModel.addNewsToHistory(news)
    }

    override fun setObserving() {
        super.setObserving()
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.newsBySection.collectLatest(adapter::submitData)
        }
    }

    override fun setListeners() {
        super.setListeners()
        binding.sectionTextView.setOnClickListener { section ->
            section.visibility = View.GONE
            arguments?.remove(KEY_SECTION)
            getNews()
        }
    }

    override fun getNews(title: String) {
        Timber.d("SectionId: ${getSectionId()}")
        getSectionId()?.let { sectionId ->
            binding.sectionTextView.apply {
                text = getString(R.string.section_name_text, sectionId)
                visibility = View.VISIBLE
            }
            viewModel.searchNewsBySectionAndTitle(sectionId = sectionId, title = title)
        } ?: super.getNews(title)
    }

    private fun getSectionId() = arguments?.getString(KEY_SECTION)

    companion object {

        private const val KEY_SECTION: String = "keySectionID"
        val TAG: String = NewsListFragment::class.java.name

        fun newInstance(sectionId: String? = null) = NewsListFragment().apply {
            arguments = bundleOf(KEY_SECTION to sectionId)
        }
    }
}