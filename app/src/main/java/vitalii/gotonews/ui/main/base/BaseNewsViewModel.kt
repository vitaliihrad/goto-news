@file:OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)

package vitalii.gotonews.ui.main.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.domain.repo.NewsRepository

typealias PagingNews = PagingData<News>

abstract class BaseNewsViewModel(private val newsRepository: NewsRepository) : ViewModel() {

    private val searchBy = MutableLiveData("")
    open val newsFlowResult: Flow<PagingNews> = searchBy.asFlow()
        .debounce(SEARCH_TIMEOUT)
        .flatMapLatest(::search)
        .cachedIn(viewModelScope)

    abstract suspend fun search(title: String): Flow<PagingNews>

    open fun addToBookmarks(news: News) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.addToBookmarks(news)
        }
    }

    open fun removeFromBookmarks(newsId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.removeFromBookmarks(newsId)
        }
    }

    open fun searchNewsByTitle(title: String) {
        viewModelScope.launch(Dispatchers.IO) {
            searchBy.postValue(title)
        }
    }

    companion object {

        const val SEARCH_TIMEOUT: Long = 500
    }
}