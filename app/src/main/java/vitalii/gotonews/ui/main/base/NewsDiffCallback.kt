package vitalii.gotonews.ui.main.base

import androidx.recyclerview.widget.DiffUtil
import vitalii.gotonews.domain.model.News

class NewsDiffCallback : DiffUtil.ItemCallback<News>() {

    override fun areItemsTheSame(oldItem: News, newItem: News) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: News, newItem: News) = oldItem == newItem
}