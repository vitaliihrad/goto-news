package vitalii.gotonews.ui.main.bookmarks

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.ui.main.base.BaseNewsViewModel
import javax.inject.Inject

@HiltViewModel
class BookmarksViewModel @Inject constructor(private val newsRepository: NewsRepository) :
    BaseNewsViewModel(newsRepository) {

    override suspend fun search(title: String) = newsRepository.getBookmarksByTitle(title)

    fun addNewsToHistory(news: News) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.addToHistory(news)
        }
    }
}