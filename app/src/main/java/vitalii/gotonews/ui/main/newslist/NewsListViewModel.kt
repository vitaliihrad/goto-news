@file:OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)

package vitalii.gotonews.ui.main.newslist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.ui.main.base.BaseNewsViewModel
import vitalii.gotonews.ui.main.base.Filter
import javax.inject.Inject

typealias PagingNews = PagingData<News>

@HiltViewModel
class NewsListViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : BaseNewsViewModel(newsRepository) {

    private val searchBySection = MutableLiveData<Filter>()
    val newsBySection: Flow<PagingNews> = searchBySection.asFlow()
        .debounce(SEARCH_TIMEOUT)
        .flatMapLatest(::searchBySection)
        .cachedIn(viewModelScope)

    override suspend fun search(title: String) = newsRepository.getNewsByTitle(title)

    fun addNewsToHistory(news: News) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.addToHistory(news)
        }
    }

    fun searchNewsBySectionAndTitle(sectionId: String?, title: String) {
        searchBySection.postValue(Filter(sectionId, title))
    }

    private fun searchBySection(filter: Filter) =
        newsRepository.getNewsBySectionIdAndTitle(filter.sectionId, filter.title)
}