package vitalii.gotonews.ui.main.history

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.ui.main.base.BaseNewsViewModel
import javax.inject.Inject

@HiltViewModel
class HistoryViewModel @Inject constructor(private val newsRepository: NewsRepository) :
    BaseNewsViewModel(newsRepository) {

    override suspend fun search(title: String) = newsRepository.getHistoryByTitle(title)

    fun removeNewsFromHistoryById(newsId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.removeFromHistory(newsId)
        }
    }
}
