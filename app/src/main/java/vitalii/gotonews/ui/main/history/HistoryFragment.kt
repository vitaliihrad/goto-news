package vitalii.gotonews.ui.main.history

import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import dagger.hilt.android.AndroidEntryPoint
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.ui.main.base.BaseNewsFragment
import vitalii.gotonews.ui.main.base.SwipeToDeleteCallback

@AndroidEntryPoint
class HistoryFragment : BaseNewsFragment() {

    override val viewModel by viewModels<HistoryViewModel>()

    private val removeCallback: (news: News?) -> Unit = {
        it?.let { news ->
            viewModel.removeNewsFromHistoryById(news.id)
        }
    }

    private val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(adapter, removeCallback))

    override fun setListeners() {
        super.setListeners()
        itemTouchHelper.attachToRecyclerView(binding.newsRecyclerView)
    }

    companion object {

        val TAG: String = HistoryFragment::class.java.name

        fun newInstance() = HistoryFragment()
    }
}