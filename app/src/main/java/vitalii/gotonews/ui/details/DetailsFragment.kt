package vitalii.gotonews.ui.details

import android.content.Intent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import coil.load
import dagger.hilt.android.AndroidEntryPoint
import vitalii.gotonews.R
import vitalii.gotonews.databinding.FragmentDetailsBinding
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.ui.base.BaseFragment
import vitalii.gotonews.ui.main.newslist.NewsListFragment
import vitalii.gotonews.util.openFragment

@AndroidEntryPoint
class DetailsFragment : BaseFragment<FragmentDetailsBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentDetailsBinding =
        FragmentDetailsBinding::inflate
    private val viewModel by viewModels<DetailsViewModel>()
    private val news: News by lazy {
        requireNotNull(arguments?.getParcelable(KEY_NEWS))
    }

    override fun setListeners() {
        binding.apply {
            section.setOnClickListener {
                openAllNewsBySection()
            }
        }
    }

    override fun initUI() {
        super.initUI()
        setHasOptionsMenu(true)
        binding.apply {
            thumbnailImageView.load(news.thumbnail)
            bodyText.text = news.bodyText
            webTitle.text = news.webTitle
            date.text = news.date
            section.text = news.section
            byline.text = news.byline
            webUrl.text = news.webUrl
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu_details, menu)
        menu.findItem(R.id.toolbar_bookmarks_tem).setOnMenuItemClickListener {
            it.setIcon(updateBookmark())
            true
        }
        menu.findItem(R.id.toolbar_share_item).setOnMenuItemClickListener {
            val intent = Intent(Intent.ACTION_SEND).apply {
                putExtra(Intent.EXTRA_TEXT, "${news.webTitle} - ${news.webUrl}")
                type = "text/plain"
            }
            startActivity(Intent.createChooser(intent, "Share Via"))
            true
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.toolbar_bookmarks_tem).setIcon(
            if (news.isBookmark) {
                R.drawable.ic_bookmark_true
            } else {
                R.drawable.ic_bookmark_false
            }
        )
        super.onPrepareOptionsMenu(menu)
    }

    private fun openAllNewsBySection() {
        openFragment(
            R.id.main_container,
            NewsListFragment.newInstance(news.sectionId),
            NewsListFragment.TAG,
            true
        )
    }

    private fun updateBookmark(): Int {
        news.isBookmark = !news.isBookmark
        return if (news.isBookmark) {
            viewModel.addToBookmarks(news = news)
            R.drawable.ic_bookmark_true
        } else {
            viewModel.removeFromBookmarks(newsId = news.id)
            R.drawable.ic_bookmark_false
        }
    }

    companion object {

        private const val KEY_NEWS: String = "keyNewsBundle"
        val TAG: String = DetailsFragment::class.java.name

        fun newInstance(news: News) = DetailsFragment().apply {
            arguments = bundleOf(KEY_NEWS to news)
        }
    }
}