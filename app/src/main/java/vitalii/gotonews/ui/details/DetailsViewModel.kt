package vitalii.gotonews.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.gotonews.domain.model.News
import vitalii.gotonews.domain.repo.NewsRepository
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel() {

    fun addToBookmarks(news: News) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.addToBookmarks(news)
        }
    }

    fun removeFromBookmarks(newsId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            newsRepository.removeFromBookmarks(newsId)
        }
    }
}