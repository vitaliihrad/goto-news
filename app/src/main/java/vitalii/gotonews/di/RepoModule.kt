package vitalii.gotonews.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import vitalii.gotonews.data.repo.MockNewsRepositoryImpl
import vitalii.gotonews.domain.repo.NewsRepository

@InstallIn(ViewModelComponent::class)
@Module
abstract class RepoModule {

    @Binds
    @ViewModelScoped
    abstract fun provideNewsRepo(repo: MockNewsRepositoryImpl): NewsRepository
}