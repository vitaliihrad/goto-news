@file:OptIn(ExperimentalCoroutinesApi::class, ExperimentalCoroutinesApi::class)

package vitalii.gotonews.ui.details

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.util.FakeData

@RunWith(AndroidJUnit4::class)
class DetailsViewModelTest {

    private val news = FakeData.getNews()
    private val newsRepository = mock(NewsRepository::class.java)
    private lateinit var detailsViewModel: DetailsViewModel

    @Before
    fun setup() = runTest {
        detailsViewModel = DetailsViewModel(newsRepository)
        Mockito.`when`(newsRepository.addToBookmarks(news)).thenReturn(Unit)
        Mockito.`when`(newsRepository.removeFromBookmarks(news.id)).thenReturn(Unit)
    }

    @After
    fun tearDown() {
        Mockito.reset(newsRepository)
    }

    @Test
    fun `add to bookmarks, news - success`() = runTest {
        detailsViewModel.addToBookmarks(news)
        delay(1000)
        Mockito.verify(newsRepository, Mockito.times(1)).addToBookmarks(news)
    }

    @Test
    fun `remove from bookmarks, news - success`() = runTest {
        detailsViewModel.removeFromBookmarks(news.id)
        delay(1000)
        Mockito.verify(newsRepository, Mockito.times(1)).removeFromBookmarks(news.id)
    }
}