@file:OptIn(ExperimentalCoroutinesApi::class, ExperimentalCoroutinesApi::class)

package vitalii.gotonews.ui.main.newslist

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.not
import org.hamcrest.Matchers.nullValue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.util.FakeData

@RunWith(AndroidJUnit4::class)
class NewsListViewModelTest {

    private val news = FakeData.getNews()
    private val newsRepository = mock(NewsRepository::class.java)
    private lateinit var newsListViewModel: NewsListViewModel

    @Before
    fun setup() = runTest {
        newsListViewModel = NewsListViewModel(newsRepository)
        Mockito.`when`(newsRepository.addToBookmarks(news)).thenReturn(Unit)
        Mockito.`when`(newsRepository.addToHistory(news)).thenReturn(Unit)
        Mockito.`when`(newsRepository.removeFromBookmarks(news.id)).thenReturn(Unit)
    }

    @After
    fun tearDown() {
        Mockito.reset(newsRepository)
    }

    @Test
    fun `add to bookmarks, news - success`() = runTest {
        newsListViewModel.addToBookmarks(news)
        delay(1000)
        Mockito.verify(newsRepository, Mockito.times(1)).addToBookmarks(news)
    }

    @Test
    fun `remove from bookmarks, news - success`() = runTest {
        newsListViewModel.removeFromBookmarks(news.id)
        delay(1000)
        Mockito.verify(newsRepository, Mockito.times(1)).removeFromBookmarks(news.id)
    }

    @Test
    fun `add to history, news - success`() = runTest {
        newsRepository.addToHistory(news)
        Mockito.verify(newsRepository, Mockito.times(1)).addToHistory(news)
    }

    @Test
    fun `search news by section and title, not empty section and title  - success`() = runTest {
        newsListViewModel.searchNewsBySectionAndTitle(news.sectionId, news.webTitle)
        val actual = newsListViewModel.newsBySection
        assertThat(actual, not(nullValue()))
    }

    @Test
    fun `search news by section and title, empty section and not empty title  - success`() =
        runTest {
            newsListViewModel.searchNewsBySectionAndTitle(null, news.webTitle)
            val actual = newsListViewModel.newsBySection
            assertThat(actual, not(nullValue()))
        }

    @Test
    fun `search news by section and title, empty section and empty title  - success`() = runTest {
        newsListViewModel.searchNewsBySectionAndTitle(null, "")
        val actual = newsListViewModel.newsBySection
        assertThat(actual, not(nullValue()))
    }

    @Test
    fun `search news by section and title, not empty section and not empty title  - success`() =
        runTest {
            newsListViewModel.searchNewsBySectionAndTitle(news.sectionId, "")
            val actual = newsListViewModel.newsBySection
            assertThat(actual, not(nullValue()))
        }

    @Test
    fun `search news by section and title, error section id and title  - success`() = runTest {
        newsListViewModel.searchNewsBySectionAndTitle("news.sectionId", "news.webTitle")
        val actual = newsListViewModel.newsBySection
        assertThat(actual, not(nullValue()))
    }
}