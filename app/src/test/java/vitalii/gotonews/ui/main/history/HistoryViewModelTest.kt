@file:OptIn(ExperimentalCoroutinesApi::class)

package vitalii.gotonews.ui.main.history

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.util.FakeData

@RunWith(AndroidJUnit4::class)
class HistoryViewModelTest {

    private val news = FakeData.getNews()
    private val newsRepository = mock(NewsRepository::class.java)
    private lateinit var historyViewModel: HistoryViewModel

    @Before
    fun setup() = runTest {
        historyViewModel = HistoryViewModel(newsRepository)
        Mockito.`when`(newsRepository.removeFromHistory(news.id)).thenReturn(Unit)
    }

    @After
    fun tearDown() {
        Mockito.reset(newsRepository)
    }

    @Test
    fun `remove news from history by id, news id - success`() = runTest {
        historyViewModel.removeNewsFromHistoryById(news.id)
        delay(1000)
        Mockito.verify(newsRepository, Mockito.times(1)).removeFromHistory(news.id)
    }
}