@file:OptIn(ExperimentalCoroutinesApi::class)

package vitalii.gotonews.ui.main.bookmarks

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import vitalii.gotonews.domain.repo.NewsRepository
import vitalii.gotonews.util.FakeData

@RunWith(AndroidJUnit4::class)
class BookmarksViewModelTest {

    private val news = FakeData.getNews()
    private val newsRepository = mock(NewsRepository::class.java)
    private lateinit var bookmarksViewModel: BookmarksViewModel

    @Before
    fun setup() = runTest {
        bookmarksViewModel = BookmarksViewModel(newsRepository)
        Mockito.`when`(newsRepository.addToHistory(news)).thenReturn(Unit)
    }

    @After
    fun tearDown() {
        Mockito.reset(newsRepository)
    }

    @Test
    fun `add news to history, news - success`() = runTest {
        bookmarksViewModel.addNewsToHistory(news)
        delay(1000)
        Mockito.verify(newsRepository, Mockito.times(1)).addToHistory(news)
    }
}