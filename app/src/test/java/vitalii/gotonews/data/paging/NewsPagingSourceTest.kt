package vitalii.gotonews.data.paging

import androidx.paging.PagingSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import vitalii.gotonews.data.repo.MockNewsRepositoryImpl
import vitalii.gotonews.util.FakeData

@ExperimentalCoroutinesApi
class NewsPagingSourceTest {

    private val newsList = FakeData.getNewsList()
    private lateinit var newsPagingSource: NewsPagingSource

    @Before
    fun setup() {
        newsPagingSource = NewsPagingSource(MockNewsRepositoryImpl.getLoader(newsList), 1)
    }

    @Test
    fun `load news, newsList - success`() = runTest {
        val expected = PagingSource.LoadResult.Page(
            data = newsList,
            prevKey = null,
            nextKey = 2
        )
        val actual = newsPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = null,
                loadSize = 1,
                placeholdersEnabled = false
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun `load news, emptyList - error`() = runTest {
        newsPagingSource = NewsPagingSource(MockNewsRepositoryImpl.getLoader(newsList), 0)
        val expected = PagingSource.LoadResult.Page(
            data = emptyList(),
            prevKey = null,
            nextKey = 2
        )
        val actual = newsPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = null,
                loadSize = 1,
                placeholdersEnabled = false
            )
        )
        assertEquals(expected, actual)
    }
}